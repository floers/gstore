use std::rc::Rc;

use grx::{
    button, component, container, grx, props, text, traits::TextExt, Component, Container, Text,
};

use crate::store::{self, store, Action, SELECT_NOTIFICATION};

#[props]
#[derive(Default)]
pub struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct State {}

#[component(State)]
pub struct Main {}

pub fn main(_: Props) -> Component {
    let txt: Rc<Text>;

    let component = grx! {
        container (styles = [p(4), vertical]) [
            "Go to menu > start",
            "Notification will be here: ",
            text(id=txt)
        ]
    };

    store().select(SELECT_NOTIFICATION, move |s| {
        if let Some(msg) = s.notification.message.as_ref() {
            txt.set_text(msg);
        } else {
            txt.set_text("");
        }
    });

    component
}
